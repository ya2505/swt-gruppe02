package connection;

import models.StudentGroup;

import java.util.List;

public class StudentGroupGetResponse extends BaseResponse<List<StudentGroup>> {
    public StudentGroupGetResponse() {
    }

    public StudentGroupGetResponse(List<StudentGroup> data) {
        super(data);
    }

    public StudentGroupGetResponse(String message) {
        super(message);
    }

    public StudentGroupGetResponse(List<StudentGroup> data, String message, boolean isSuccess) {
        super(data, message, isSuccess);
    }
}
