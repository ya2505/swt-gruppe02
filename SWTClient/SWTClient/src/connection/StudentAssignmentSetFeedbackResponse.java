package connection;

public class StudentAssignmentSetFeedbackResponse extends BaseResponse<Void> {
    public StudentAssignmentSetFeedbackResponse() {
    }

    public StudentAssignmentSetFeedbackResponse(Void data) {
        super(data);
    }

    public StudentAssignmentSetFeedbackResponse(String message) {
        super(message);
    }

    public StudentAssignmentSetFeedbackResponse(Void data, String message, boolean isSuccess) {
        super(data, message, isSuccess);
    }
}
