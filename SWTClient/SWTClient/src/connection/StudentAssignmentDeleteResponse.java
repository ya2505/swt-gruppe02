package connection;

public class StudentAssignmentDeleteResponse extends BaseResponse<Void> {

    public StudentAssignmentDeleteResponse() {
    }

    public StudentAssignmentDeleteResponse(Void data) {
        super(data);
    }

    public StudentAssignmentDeleteResponse(String message) {
        super(message);
    }

    public StudentAssignmentDeleteResponse(Void data, String message, boolean isSuccess) {
        super(data, message, isSuccess);
    }
}
