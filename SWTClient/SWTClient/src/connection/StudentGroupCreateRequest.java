package connection;

import java.util.List;

public class StudentGroupCreateRequest extends BaseRequest {
    private int teacherId;
    private String name;
    private List<Integer> studentIds;

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public StudentGroupCreateRequest(int teacherId, String name, List<Integer> studentIds) {
        this.teacherId = teacherId;
        this.name = name;
        this.studentIds = studentIds;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Integer> getStudentIds() {
        return studentIds;
    }

    public void setStudentIds(List<Integer> studentIds) {
        this.studentIds = studentIds;
    }
}
