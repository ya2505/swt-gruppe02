package connection;

import models.Person;

import java.util.List;

public class GetAllStudentsResponse extends BaseResponse<List<Person>> {

    public GetAllStudentsResponse() {
    }

    public GetAllStudentsResponse(List<Person> data) {
        super(data);
    }

    public GetAllStudentsResponse(String message) {
        super(message);
    }

    public GetAllStudentsResponse(List<Person> data, String message, boolean isSuccess) {
        super(data, message, isSuccess);
    }
}
