package connection;

import java.util.List;

public class StudentAssignmentCreateRequest extends BaseRequest {
    private List<Integer> groupIds;
    private String text;
    private int teacherId;

    public StudentAssignmentCreateRequest(List<Integer> groupIds, String text, int teacherId) {
        this.groupIds = groupIds;
        this.text = text;
        this.teacherId = teacherId;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public List<Integer> getGroupIds() {
        return groupIds;
    }

    public void setGroupIds(List<Integer> groupIds) {
        this.groupIds = groupIds;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
