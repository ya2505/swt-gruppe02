package connection;

public class StudentGroupCreateResponse extends BaseResponse<String> {

    public StudentGroupCreateResponse() {
    }
    public StudentGroupCreateResponse(String message) {
        super(null, message,false);
    }

    public StudentGroupCreateResponse(String data, String message, boolean isSuccess) {
        super(data, message, isSuccess);
    }
}
