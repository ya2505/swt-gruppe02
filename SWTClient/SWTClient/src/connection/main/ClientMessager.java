package connection.main;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import connection.*;
import general.Pair;

public class ClientMessager implements Runnable {
	private Socket socket;
	private ObjectInputStream in;
	private ObjectOutputStream out;
	
	public ClientMessager(int port) {
		try {
			socket = new Socket("127.0.0.1", port);
			out = new ObjectOutputStream(socket.getOutputStream());
	        in = new ObjectInputStream(socket.getInputStream());
		} catch (Exception e) { e.printStackTrace(); }
	}


	public <T,T2> T send(T2 request) {
		try {
			out.writeObject(request);
			return (T) in.readObject();
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}

	
	public Pair<Boolean, Object> getData(String type, String args) {

		try {
			return new Pair<>(false, null);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public Pair<Boolean, String> setData(String type, String args, String data) {
		try {
			return new Pair<>(false, null);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
	}
	
	@Override
	public void run() {}
}
