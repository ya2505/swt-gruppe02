package connection;

public class BaseResponse<T> extends Response {
    protected T data;
    protected String message;
    protected boolean isSuccess;

    public BaseResponse() {
        this(null,null,true);
    }
    public BaseResponse(T data) {
        this(data, null, true);
    }

    public BaseResponse(String message) {
        this(null, message, false);
    }

    public BaseResponse(T data, String message, boolean isSuccess) {
        this.data = data;
        this.message = message;
        this.isSuccess = isSuccess;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return isSuccess;
    }

    public void setSuccess(boolean success) {
        isSuccess = success;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }
}
