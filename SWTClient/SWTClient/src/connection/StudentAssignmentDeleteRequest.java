package connection;

public class StudentAssignmentDeleteRequest extends BaseRequest {
    private int id;

    public StudentAssignmentDeleteRequest(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
