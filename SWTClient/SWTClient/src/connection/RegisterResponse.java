package connection;

import models.Person;


public class RegisterResponse extends BaseResponse<Person> {
    public RegisterResponse() {
        super();
    }

    public RegisterResponse(Person data) {
        super(data);
    }

    public RegisterResponse(String message) {
        super(message);
    }

    public RegisterResponse(Person data, String message, boolean isSuccess) {
        super(data, message, isSuccess);
    }
}
