package connection;

import models.StudentAssignment;

import java.util.List;

public class StudentAssignmentViewResponse extends BaseResponse<List<StudentAssignment>> {

    public StudentAssignmentViewResponse() {
    }

    public StudentAssignmentViewResponse(List<StudentAssignment> data) {
        super(data);
    }

    public StudentAssignmentViewResponse(String message) {
        super(message);
    }

    public StudentAssignmentViewResponse(List<StudentAssignment> data, String message, boolean isSuccess) {
        super(data, message, isSuccess);
    }
}
