package connection;

public class GetStudentsByGroupIdRequest extends BaseRequest {
    private int groupId;

    public GetStudentsByGroupIdRequest(int groupId) {
        this.groupId = groupId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }
}
