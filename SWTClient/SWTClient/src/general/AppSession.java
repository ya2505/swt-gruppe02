package general;

import models.Person;

public class AppSession
{
    public static Person Person = null;

    public static void Login(Person person) {
        AppSession.Person = person;
    }
    public static void LogOut() {
        AppSession.Person = null;
    }

}
