package general;

import connection.main.ClientMessager;
import gui.main.LoginForm;

import javax.swing.*;

public class Run {
	public static ClientMessager msg = new ClientMessager(3333);
	
	public static void main(String[] args) {
		//new Thread(msg).start();
		JFrame frame = new JFrame("Login");
		frame.setContentPane(new LoginForm().loginPanel);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.pack();
		frame.setLocationRelativeTo(null);
		frame.setResizable(false);
		frame.setSize(400,300);
		frame.setVisible(true);

	}
}
