package models;

import javax.swing.table.AbstractTableModel;
import java.util.ArrayList;
import java.util.List;

public class StudentAssignmentTableModel extends AbstractTableModel {

    private List<StudentAssignment> data;
    private String[] columns ;

    public StudentAssignmentTableModel(List<StudentAssignment> data) {
        this.data = data;
        columns = new String[] {"Student", "Text","Answer", "FeedBack","IsComplete"};
    }

    public List<StudentAssignment> getData() {
        return data;
    }

    public void setData(ArrayList<StudentAssignment> data) {
        this.data = data;
    }

    @Override
    public int getRowCount() {
        return data.size();
    }

    @Override
    public int getColumnCount() {
        return columns.length;
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        var d = this.data.get(rowIndex);
        switch (columnIndex) {
            case 0:
                return d.getName();
            case 1:
                return d.getText();
            case 2:
                return d.getAnswer();
            case 3:
                return d.getFeedback();
            case 4:
                return d.isComplete();

        }
        return null;
    }

    public String getColumnName(int col) {
        return columns[col] ;
    }
}
