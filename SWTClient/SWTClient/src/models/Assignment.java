package models;

import connection.Response;

public class Assignment extends Response {
    private int id;
    private String text;
    private int groupId;
    private String name;

    public Assignment(int id, String text, int groupId, String name) {
        this.id = id;
        this.text = text;
        this.groupId = groupId;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name + " - " + text;
    }
}
