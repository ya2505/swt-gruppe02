package gui.main;

import gui.main.teacher.TeacherAssignmentForm;
import gui.main.teacher.TeacherGroupForm;

import javax.swing.*;

public class TeacherDashboardForm  extends SharedForm {
    public JPanel main;
    private JTabbedPane tabbedPanelMain;

    public TeacherDashboardForm() {
        tabbedPanelMain.addTab("Groups",new TeacherGroupForm().main);
        tabbedPanelMain.addTab("Assignments",new TeacherAssignmentForm().main);
        tabbedPanelMain.addTab("LogOut",new LogOutForm().main);
    }
}
