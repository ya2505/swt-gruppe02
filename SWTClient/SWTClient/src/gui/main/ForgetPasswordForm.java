package gui.main;

import connection.ForgetPasswordRequest;
import connection.ForgetPasswordResponse;
import connection.RegisterRequest;
import general.Run;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class ForgetPasswordForm extends SharedForm {
    public JPanel panel;
    private JTextField txtEmail;
    private JPasswordField txtPassword;
    private JButton resetPasswordButton;
    private JButton cancelButton;

    public ForgetPasswordForm() {
        cancelButton.addActionListener(e -> {
            setJFrame(e);
            jFrame.setTitle("CaseMaster - Login");
            jFrame.setContentPane(new LoginForm().loginPanel);
            jFrame.pack();
            jFrame.setLocationRelativeTo(null);
            jFrame.setSize(400, 300);
            jFrame.setVisible(true);
        });
        resetPasswordButton.addActionListener(e -> {
            setJFrame(e);
            ForgetPasswordResponse response = Run.msg.send(new ForgetPasswordRequest(txtEmail.getText(), String.valueOf(txtPassword.getPassword())));
            if (response.isSuccess()) {
                JOptionPane.showMessageDialog(jFrame, "Password reset successfully");
                txtPassword.setText("");
                txtEmail.setText("");
            } else {
                JOptionPane.showMessageDialog(jFrame, response.getMessage());
            }
        });
    }
}
