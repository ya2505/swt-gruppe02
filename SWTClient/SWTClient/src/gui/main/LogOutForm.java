package gui.main;

import general.AppSession;

import javax.swing.*;

public class LogOutForm  extends SharedForm {
    public JPanel main;
    private JButton logoutButton;

    public LogOutForm() {
        logoutButton.addActionListener(e -> {
            setJFrame(e);
            AppSession.LogOut();
            jFrame.setTitle("CaseMaster - Login");
            jFrame.setContentPane(new LoginForm().loginPanel);
            jFrame.pack();
            jFrame.setLocationRelativeTo(null);
            jFrame.setSize(400,300);
            jFrame.setVisible(true);
        });
    }
}
