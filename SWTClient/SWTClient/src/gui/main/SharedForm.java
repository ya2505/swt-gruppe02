package gui.main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class SharedForm {
    protected JFrame jFrame;

    protected void setJFrame(ActionEvent e) {
        if (jFrame == null) {
            Component component = (Component) e.getSource();
            // Returns the root component for the current component tree
            jFrame = (JFrame) SwingUtilities.getRoot(component);
        }
    }
}
