package gui.main.student;

import connection.StudentGroupGetRequest;
import connection.StudentGroupGetResponse;
import general.AppSession;
import general.Run;
import gui.main.teacher.TeacherGroupViewDialog;
import models.StudentGroup;

import javax.swing.*;

public class StudentGroupForm extends JFrame {
    public JPanel main;
    private JButton viewButton;
    private JList<StudentGroup> groupList;

    public StudentGroupForm() {

        viewButton.addActionListener(e -> {
            var items = this.groupList.getSelectedValuesList();
            if (items.isEmpty()) {
                return;
            }
            TeacherGroupViewDialog dialog = new TeacherGroupViewDialog(items.get(0).getId());
            dialog.pack();
            dialog.setSize(300,300);
            dialog.setVisible(true);
        });
        getData();
    }
    private void getData() {
        StudentGroupGetResponse response = Run.msg.send(new StudentGroupGetRequest(0, AppSession.Person.getId()));
        if (response.isSuccess()) {
            var data = response.getData();
            var model = new DefaultListModel<StudentGroup>();
            model.addAll(data);
            this.groupList.setModel(model);
        }
    }

}
