package gui.main.student;

import connection.*;
import general.AppSession;
import general.Run;
import models.StudentAssignment;
import models.StudentAssignmentTableModel;

import javax.swing.*;
import java.util.List;

public class StudentAssignmentForm extends JFrame {
    private JButton setCompleteButton;
    private JTable assignmentTable;
    public JPanel main;
    private List<StudentAssignment> data;

    public StudentAssignmentForm() {

        setCompleteButton.addActionListener(e -> {
            var selectedRow = assignmentTable.getSelectedRow();
            if (selectedRow > -1) {
                var d = this.data.get(selectedRow);
                var message = JOptionPane.showInputDialog(this,"Enter your Answer");
                if (message.isEmpty()) {
                    return;
                }
                StudentAssignmentSetCompleteResponse response = Run.msg.send(new StudentAssignmentSetCompleteRequest(d.getId(),message));
                if (response.isSuccess()) {
                    JOptionPane.showMessageDialog(this,"Complete Saved");
                    getData();
                }
                else {
                    JOptionPane.showMessageDialog(this,response.getMessage());
                }
            }
        });

        getData();
    }

    private void getData() {
        StudentAssignmentViewResponse response =  Run.msg.send(new StudentAssignmentViewRequest(0, AppSession.Person.getId()));
        if (response.isSuccess()) {
            data = response.getData();
            assignmentTable.setModel(new StudentAssignmentTableModel(data));
        }
        else {
            JOptionPane.showMessageDialog(this,response.getMessage());
        }
    }

}
