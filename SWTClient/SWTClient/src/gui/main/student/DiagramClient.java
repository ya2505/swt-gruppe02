package gui.main.student;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.util.ArrayList;
import java.util.List;

public class DiagramClient {
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            JFrame frame = new JFrame("UML Use Case Diagram");
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

            DiagramPanel diagramPanel = new DiagramPanel();
            frame.add(diagramPanel, BorderLayout.CENTER);

            // Create the Actor button
            JToolBar toolBar = new JToolBar();
            JButton actorButton = new JButton("Actor");
            actorButton.addActionListener(e -> diagramPanel.setDrawingMode(DiagramPanel.DrawingMode.ACTOR));
            toolBar.add(actorButton);

            // Create the Use Case button
            JButton useCaseButton = new JButton("Use Case");
            useCaseButton.addActionListener(e -> diagramPanel.setDrawingMode(DiagramPanel.DrawingMode.USE_CASE));
            toolBar.add(useCaseButton);

            // Create the line button
            JButton lineButton = new JButton("Line");
            lineButton.addActionListener(e -> diagramPanel.setDrawingMode(DiagramPanel.DrawingMode.LINE));
            toolBar.add(lineButton);

            // Create the text button
            JButton textButton = new JButton("Text");
            textButton.addActionListener(e -> diagramPanel.setDrawingMode(DiagramPanel.DrawingMode.TEXT));
            toolBar.add(textButton);

            // Add delete button
            JButton deleteButton = new JButton("Delete");
            deleteButton.addActionListener(e -> diagramPanel.setDrawingMode(DiagramPanel.DrawingMode.DELETE));
            toolBar.add(deleteButton);

            frame.add(toolBar, BorderLayout.NORTH);

            frame.setSize(800, 600);
            frame.setLocationRelativeTo(null);
            frame.setVisible(true);
        });
    }

    public class MainFrame extends JFrame {

        public MainFrame() {
            setTitle("UML Diagram App");
            setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            setSize(400, 300);
            setLocationRelativeTo(null);

            JButton runButton = new JButton("Run Diagram");
            runButton.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    // Run the DiagramClient code
                    DiagramClient.main(new String[0]);
                }
            });

            JPanel panel = new JPanel();
            panel.add(runButton);

            setContentPane(panel);
        }

        public void main(String[] args) {
            SwingUtilities.invokeLater(new Runnable() {
                @Override
                public void run() {
                    MainFrame frame = new MainFrame();
                    frame.setVisible(true);
                }
            });
        }
    }

    public static class DiagramPanel extends JPanel {
        private DrawingMode drawingMode = DrawingMode.NONE;// Defining drawingMode variable
        private Actor selectedActor = null;// Defining selectedActor
        private java.util.List<Actor> actors = new ArrayList<>();// Defining actors list
        private java.util.List<UseCase> useCases = new ArrayList<>();// Defining useCases list
        private Line currentLine = null;// Defining currentLine
        private java.util.List<Line> lines = new ArrayList<>();// Defining lines list
        // Add textShapes list
        private List<TextShape> textShapes = new ArrayList<>();// Defining textShapes list





        public DiagramPanel() {// Constructor
            setBackground(Color.WHITE);// Set background color
            addMouseListener(new MouseAdapter() {// Adding mouseListener

                @Override
                public void mouseClicked(MouseEvent e) {// Defining mouseClicked method
                    super.mouseClicked(e);
                    // Checking if left mouse button is clicked in TEXT mode
                    if (drawingMode == DrawingMode.TEXT && SwingUtilities.isLeftMouseButton(e)) {
                        // Show input dialog to enter text
                        String text = JOptionPane.showInputDialog("Enter text:");
                        if(text != null) {
                            // Create TextShape object
                            TextShape textShape = new TextShape(e.getX(), e.getY(), text);
                            textShapes.add(textShape);// Adding textShape to textShapes list
                            repaint();// Repainting
                        }
                    }
                    // Add DELETE case to mouseClicked
                    if (drawingMode == DrawingMode.DELETE) {// Checking if in DELETE mode
                        // Check if an actor was clicked
                        for (Actor actor : actors) {
                            if (actor.containsHead(e.getX(), e.getY())) {
                                actors.remove(actor);// Removing actor
                                repaint();
                                break;
                            }
                        }
                        // Check if a use case was clicked
                        for (UseCase useCase : useCases) {
                            if (useCase.containsBody(e.getX(), e.getY())) {
                                useCases.remove(useCase);// Removing useCase
                                repaint();
                                break;
                            }
                        }
                        // Check if a line was clicked
                        for (Line line : lines) {
                            // Use a threshold of 10 pixels for clicking a line
                            if (Math.abs(line.x1 - e.getX()) < 10 && Math.abs(line.y1 - e.getY()) < 10) {
                                lines.remove(line);// Removing line
                                repaint();
                                break;
                            }
                            if (Math.abs(line.x2 - e.getX()) < 10 && Math.abs(line.y2 - e.getY()) < 10) {
                                lines.remove(line);
                                repaint();
                                break;
                            }
                        }
                        // Check if a text shape was clicked
                        for (TextShape textShape : textShapes) {
                            // Use a threshold of 10 pixels for clicking a text shape
                            if (Math.abs(textShape.x - e.getX()) < 10 &&
                                    Math.abs(textShape.y - e.getY()) < 10) {
                                textShapes.remove(textShape);
                                repaint();
                                break;
                            }
                        }
                    }
                    if (SwingUtilities.isRightMouseButton(e)) {// Checking if right mouse button is clicked
                        // Right-click event
                        boolean actorFound = false;
                        for (Actor actor : actors) {
                            if (actor.containsHead(e.getX(), e.getY())) {
                                // Show input dialog to edit actor label
                                String newLabel = JOptionPane.showInputDialog("Enter new label for actor", actor.getLabel());
                                if (newLabel != null) {
                                    actor.setLabel(newLabel);// Setting new label for actor
                                    selectedActor = actor;// Setting selected actor
                                    repaint();
                                }
                                actorFound = true;
                                break;
                            }
                        }
                        if (!actorFound) {
                            // Check for use case
                            for (UseCase useCase : useCases) {
                                if (useCase.containsBody(e.getX(), e.getY())) {
                                    // Show input dialog to edit use case label
                                    String newLabel = JOptionPane.showInputDialog("Enter new label for use case", useCase.getLabel());
                                    if (newLabel != null) {
                                        useCase.setLabel(newLabel);
                                        repaint();
                                    }
                                    break;
                                }
                            }
                        }
                    } else if (e.getClickCount() == 2) {
                        // Double-click event
                        for (Actor actor : actors) {
                            if (actor.containsHead(e.getX(), e.getY())) {
                                selectedActor = actor;
                                break;
                            }
                        }
                    } else if (drawingMode == DrawingMode.ACTOR) {
                        drawActor(e.getX(), e.getY(), "Actor");
                    } else if (drawingMode == DrawingMode.USE_CASE) {
                        drawUseCase(e.getX(), e.getY(), "Use Case");
                    }else if (drawingMode == DrawingMode.LINE && SwingUtilities.isLeftMouseButton(e)) {
                        // Start or finish a line
                        if (currentLine == null) {
                            currentLine = new Line(e.getX(), e.getY(), e.getX(), e.getY());
                        } else {
                            lines.add(currentLine);// Adding line to lines list
                            currentLine = null;
                            repaint();
                        }
                    }
                }

                @Override
                public void mousePressed(MouseEvent e) {
                    super.mousePressed(e);
                    if (SwingUtilities.isLeftMouseButton(e)) {
                        // Left-click event
                        if (selectedActor != null) {
                            int dx = e.getX() - selectedActor.getX();
                            int dy = e.getY() - selectedActor.getY();
                            selectedActor.setOffset(dx, dy);
                        }
                    }
                }

                @Override
                public void mouseReleased(MouseEvent e) {
                    super.mouseReleased(e);
                    if (SwingUtilities.isLeftMouseButton(e)) {
                        // Left-click event
                        if (selectedActor != null) {
                            selectedActor.resetOffset();
                            selectedActor = null;
                            repaint();
                        }
                    }
                }
            });

            addMouseMotionListener(new MouseAdapter() {
                @Override
                public void mouseDragged(MouseEvent e) {
                    super.mouseDragged(e);
                    if (SwingUtilities.isLeftMouseButton(e)) {
                        // Left-click event
                        if (selectedActor != null) {
                            int x = e.getX() - selectedActor.getDx();
                            int y = e.getY() - selectedActor.getDy();
                            selectedActor.setPosition(x, y);
                            repaint();
                        }else if (drawingMode == DrawingMode.LINE && currentLine != null) {
                            // Update the current line's end point while the mouse is being dragged
                            currentLine.setEnd(e.getX(), e.getY());
                            repaint();
                        }
                    }
                }
            });
        }
        // Add system boundary
        private void drawSystemBoundary(Graphics2D g2d) {
            // Set the background color to baby blue
            g2d.setColor(new Color(137,207,240));
            g2d.fillRect(100, 50, getWidth() - 200, getHeight() - 100);

            // Draw the black rectangle
            g2d.setColor(Color.BLACK);
            g2d.drawRect(100, 50, getWidth() - 200, getHeight() - 100);
        }

        public void setDrawingMode(DrawingMode drawingMode) {
            this.drawingMode = drawingMode;
        }

        private void drawActor(int x, int y, String name) {
            Actor actor = new Actor(x, y, name);
            actors.add(actor);
            selectedActor = actor;
            repaint();
        }

        private void drawUseCase(int x, int y, String name) {
            UseCase useCase = new UseCase(x, y, name);
            useCases.add(useCase);
            repaint();
        }

        public enum DrawingMode {
            NONE,
            ACTOR,
            USE_CASE,
            LINE,
            DELETE,
            TEXT
        }

        private class Actor {
            private int x, y;
            private String label;
            private int dx, dy;

            public Actor(int x, int y, String label) {
                this.x = x;
                this.y = y;
                this.label = label;
            }

            public void draw(Graphics2D g2d) {
                // Set the background color of the head ellipse to baby blue
                g2d.setColor(new Color(137, 207, 240));
                g2d.fill(new Ellipse2D.Double(x - 10, y, 20, 20));

                // Draw actor
                g2d.setColor(Color.BLACK);
                g2d.draw(new Ellipse2D.Double(x - 10, y, 20, 20));
                g2d.drawLine(x, y + 20, x, y + 50);
                g2d.drawLine(x - 15, y + 30, x + 15, y + 30);
                g2d.drawLine(x, y + 50, x - 10, y + 80);
                g2d.drawLine(x, y + 50, x + 10, y + 80);

                // Draw label
                FontMetrics fm = g2d.getFontMetrics();
                int labelWidth = fm.stringWidth(label);
                g2d.drawString(label, x - labelWidth / 2, y + 95);
            }

            public boolean containsHead(int x, int y) {
                return new Ellipse2D.Double(this.x - 10, this.y, 20, 20).contains(x, y);
            }

            public void setPosition(int x, int y) {
                this.x = x;
                this.y = y;
            }

            public void setOffset(int dx, int dy) {
                this.dx = dx;
                this.dy = dy;
            }

            public void resetOffset() {
                dx = 0;
                dy = 0;
            }

            public int getX() {
                return x;
            }

            public int getY() {
                return y;
            }

            public int getDx() {
                return dx;
            }

            public int getDy() {
                return dy;
            }

            public String getLabel() {
                return label;
            }

            public void setLabel(String label) {
                this.label = label;
            }
        }

        private class UseCase {
            private int x, y;
            private String label;

            public UseCase(int x, int y, String label) {
                this.x = x;
                this.y = y;
                this.label = label;
            }

            public void draw(Graphics2D g2d) {
                // Draw use case
                g2d.draw(new Ellipse2D.Double(x - 50, y - 30, 100, 60));

                // Draw label
                FontMetrics fm = g2d.getFontMetrics();
                int labelWidth = fm.stringWidth(label);
                g2d.drawString(label, x - labelWidth / 2, y + 5);
            }

            public boolean containsBody(int x, int y) {
                return new Ellipse2D.Double(this.x - 50, this.y - 30, 100, 60).contains(x, y);
            }

            public String getLabel() {
                return label;
            }

            public void setLabel(String label) {
                this.label = label;
            }
        }
        private class Line {
            private int x1, y1, x2, y2;

            public Line(int x1, int y1, int x2, int y2) {
                this.x1 = x1;
                this.y1 = y1;
                this.x2 = x2;
                this.y2 = y2;
            }

            public void draw(Graphics2D g2d) {
                g2d.drawLine(x1, y1, x2, y2);
            }

            public void setEnd(int x, int y) {
                x2 = x;
                y2 = y;
            }
        }
        private class TextShape {
            int x;
            int y;
            String text;

            public TextShape(int x, int y, String text) {
                this.x = x;
                this.y = y;
                this.text = text;
            }

            public void draw(Graphics2D g2d) {

                FontMetrics fm = g2d.getFontMetrics();

                int width = fm.stringWidth(text);
                int height = fm.getHeight();

                int halfWidth = width / 2;
                int halfHeight = height / 2;

                //optional text border
                //g2d.drawRect(x - halfWidth - 2, y - halfHeight + 2,width + 4, height + 4);

                g2d.setColor(Color.BLACK);

                // Add half height to y
                g2d.drawString(text, x - halfWidth, y + halfHeight);
            }
        }


        @Override
        public void paintComponent(Graphics g) {
            super.paintComponent(g);
            Graphics2D g2d = (Graphics2D) g;
            drawSystemBoundary(g2d);  // Add call to draw system boundary

            // Draw actors
            for (Actor actor : actors) {
                actor.draw(g2d);
            }

            // Draw use cases
            for (UseCase useCase : useCases) {
                useCase.draw(g2d);
            }
            // Draw lines
            for (Line line : lines) {
                line.draw(g2d);
            }
            // Draw text shapes
            for (TextShape textShape : textShapes) {
                textShape.draw(g2d);
            }
        }

    }
}
