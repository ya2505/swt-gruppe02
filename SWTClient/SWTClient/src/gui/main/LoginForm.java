package gui.main;

import connection.LoginRequest;
import connection.LoginResponse;
import general.AppSession;
import general.Run;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class LoginForm extends SharedForm {
    private JTextField txtEmail;
    private JPasswordField txtPassword;
    private JButton loginButton;
    private JButton registerButton;
    private JButton btnCancel;
    public JPanel loginPanel;
    private JButton forgetPasswordButton;


    public LoginForm() {
        btnCancel.addActionListener(e -> {
            setJFrame(e);
            jFrame.dispose();
        });
        loginButton.addActionListener(e -> {
            setJFrame(e);
            LoginResponse r = Run.msg.send(new LoginRequest(txtEmail.getText(),String.valueOf(txtPassword.getPassword())));
            if (r.isSuccess()) {
                JOptionPane.showMessageDialog(jFrame,"Login Success");
                AppSession.Login(r.getData());
                if (AppSession.Person.getRole().equals("Teacher")) {
                    jFrame.setTitle("CaseMaster - Teacher");
                    jFrame.setContentPane(new TeacherDashboardForm().main);
                }
                else {
                    jFrame.setTitle("CaseMaster - Student");
                    jFrame.setContentPane(new StudentDashboardForm().main);
                }
                jFrame.pack();
                jFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
                jFrame.setLocationRelativeTo(null);
                jFrame.setVisible(true);
            }
            else {
                JOptionPane.showMessageDialog(jFrame,r.getMessage());
            }
        });
        registerButton.addActionListener(e -> {
            setJFrame(e);
            jFrame.setTitle("CaseMaster - Register");
            jFrame.setContentPane(new RegisterDialog().registerPanel);
            jFrame.pack();
            jFrame.setSize(400,300);
            jFrame.setLocationRelativeTo(null);
            jFrame.setVisible(true);
        });
        forgetPasswordButton.addActionListener(e -> {
            setJFrame(e);
            jFrame.setTitle("CaseMaster - Forget Password");
            jFrame.setContentPane(new ForgetPasswordForm().panel);
            jFrame.pack();
            jFrame.setSize(400,300);
            jFrame.setLocationRelativeTo(null);
            jFrame.setVisible(true);
        });
    }
}
