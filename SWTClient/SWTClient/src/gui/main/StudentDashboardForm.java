package gui.main;
import gui.main.student.StudentAssignmentForm;
import gui.main.student.StudentGroupForm;
import gui.main.student.StudentUmlForm;

import javax.swing.*;

public class StudentDashboardForm  extends SharedForm {
    private JTabbedPane tabPanel;
    public JPanel main;

    public StudentDashboardForm() {

        tabPanel.addTab("Groups",new StudentGroupForm().main);
        tabPanel.addTab("Assignment",new StudentAssignmentForm().main);
        tabPanel.addTab("UML",new StudentUmlForm().main);
        tabPanel.addTab("LogOut",new LogOutForm().main);
    }
}
