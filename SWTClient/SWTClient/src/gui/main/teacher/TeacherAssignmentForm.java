package gui.main.teacher;

import connection.*;
import general.AppSession;
import general.Run;
import models.Assignment;

import javax.swing.*;

public class TeacherAssignmentForm {
    private JButton createButton;
    private JButton viewButton;
    private JButton deleteButton;
    private JList<Assignment> assignmentList;
    public JPanel main;

    public TeacherAssignmentForm() {

        createButton.addActionListener(e -> {
            TeacherAssignmentCreateDialog dialog = new TeacherAssignmentCreateDialog();
            dialog.pack();
            dialog.setSize(300,300);
            dialog.setVisible(true);
            this.getData();
        });
        deleteButton.addActionListener(e -> {
            var data = this.assignmentList.getSelectedValuesList();
            if (data.isEmpty()) {
                return;
            }
            var result = JOptionPane.showConfirmDialog(this.main,"Do you want to Delete?","Confirm",JOptionPane.OK_CANCEL_OPTION);
            if (result == 0) {
                StudentAssignmentDeleteResponse res = Run.msg.send(new StudentAssignmentDeleteRequest(data.get(0).getId()));
                if (res.isSuccess()) {
                    getData();
                }
                else {
                    JOptionPane.showMessageDialog(this.main,res.getMessage());
                }
            }
        });
        viewButton.addActionListener(e -> {
            var data = this.assignmentList.getSelectedValuesList();
            if (data.isEmpty()) {
                return;
            }
            TeacherAssignmentViewDialog dialog = new TeacherAssignmentViewDialog(data.get(0).getId());
            dialog.pack();
            dialog.setSize(800,600);
            dialog.setVisible(true);
            this.getData();
        });

        getData();
    }


    private void  getData() {
        StudentAssignmentViewByGroupResponse response = Run.msg.send(new StudentAssignmentViewByGroup(AppSession.Person.getId()));
        if (!response.isSuccess()) {
            JOptionPane.showMessageDialog(main,response.getMessage());
            return;
        }
        var data = response.getData();
        var model = new DefaultListModel<Assignment>();
        model.addAll(data);
        this.assignmentList.setModel(model);
    }
}
