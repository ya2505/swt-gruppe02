package gui.main.teacher;

import connection.GetAllStudentsRequest;
import connection.GetAllStudentsResponse;
import connection.StudentGroupCreateRequest;
import connection.StudentGroupCreateResponse;
import general.AppSession;
import general.Run;
import gui.main.renderer.CheckboxListCellRenderer;
import models.Person;

import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

public class TeacherGroupCreateDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField txtName;
    private JList<Person> studentList;
    private List<Person> students = new ArrayList<>();

    public TeacherGroupCreateDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);
        setTitle("CaseMaster - Create Group");

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

       GetAllStudentsResponse response =  Run.msg.send(new GetAllStudentsRequest());
       if (!response.isSuccess()) {
           JOptionPane.showMessageDialog(this.contentPane,response.getMessage());
       }
       else {
           students = response.getData();
           var model = new DefaultListModel<Person>();
           model.addAll(students);
           this.studentList.setModel(model);
           this.studentList.setCellRenderer(new CheckboxListCellRenderer());
           this.studentList.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
       }
    }

    private void onOK() {
        var studentIds = this.studentList.getSelectedValuesList().stream().map(Person::getId).toList();
        var req = new StudentGroupCreateRequest(AppSession.Person.getId(),txtName.getText(),studentIds);
        StudentGroupCreateResponse res = Run.msg.send(req);
        if (res.isSuccess()) {
            JOptionPane.showMessageDialog(this.contentPane,"Group Created");
            dispose();
        }
        else {
            JOptionPane.showMessageDialog(this.contentPane,res.getMessage());
        }
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }
}
