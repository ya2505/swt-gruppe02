package gui.main.teacher;

import connection.GetStudentsByGroupIdRequest;
import connection.GetStudentsByGroupIdResponse;
import general.Run;
import models.Person;

import javax.swing.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.List;

public class TeacherGroupViewDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonCancel;
    private JList<Person> studentList;
    private int  groupId;
    private List<Person> students = new ArrayList<>();
    public TeacherGroupViewDialog(int groupId) {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonCancel);
        setTitle("CaseMaster - View Students in Group");

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        GetStudentsByGroupIdResponse response =  Run.msg.send(new GetStudentsByGroupIdRequest(groupId));
        if (!response.isSuccess()) {
            JOptionPane.showMessageDialog(this.contentPane,response.getMessage());
        }
        else {
            students = response.getData();
            var model = new DefaultListModel<Person>();
            model.addAll(students);
            this.studentList.setModel(model);
        }
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }
}
