package gui.main.teacher;

import connection.StudentAssignmentCreateRequest;
import connection.StudentAssignmentCreateResponse;
import connection.StudentGroupGetRequest;
import connection.StudentGroupGetResponse;
import general.AppSession;
import general.Run;
import models.StudentGroup;

import javax.swing.*;
import java.awt.event.*;

public class TeacherAssignmentCreateDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JButton buttonCancel;
    private JTextField txtText;
    private JList<StudentGroup> groupList;

    public TeacherAssignmentCreateDialog() {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onOK();
            }
        });

        buttonCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        });

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);

        getData();
    }

    private void getData() {
        StudentGroupGetResponse response = Run.msg.send(new StudentGroupGetRequest(AppSession.Person.getId(), 0));
        if (response.isSuccess()) {
            var data = response.getData();
            var model = new DefaultListModel<StudentGroup>();
            model.addAll(data);
            this.groupList.setModel(model);
            this.groupList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
            this.groupList.setCellRenderer(new gui.main.renderer.CheckboxListCellRenderer());
        }
    }
    private void onOK() {
        var groups = this.groupList.getSelectedValuesList().stream().map(StudentGroup::getId).toList();
        StudentAssignmentCreateResponse response = Run.msg.send(new StudentAssignmentCreateRequest(groups,txtText.getText(),AppSession.Person.getId()));
        if (response.isSuccess()) {
            dispose();
        }
        else {
            JOptionPane.showMessageDialog(this,response.getMessage());
        }
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }
}
