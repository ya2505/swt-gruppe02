package gui.main.teacher;

import connection.StudentAssignmentSetFeedbackRequest;
import connection.StudentAssignmentSetFeedbackResponse;
import connection.StudentAssignmentViewRequest;
import connection.StudentAssignmentViewResponse;
import general.Run;
import models.StudentAssignment;
import models.StudentAssignmentTableModel;

import javax.swing.*;
import java.awt.event.*;
import java.util.List;

public class TeacherAssignmentViewDialog extends JDialog {
    private JPanel contentPane;
    private JButton buttonOK;
    private JTable assignmentTable;
    private JButton feedbackButton;
    private List<StudentAssignment> data;

    public TeacherAssignmentViewDialog(int assignmentId) {
        setContentPane(contentPane);
        setModal(true);
        getRootPane().setDefaultButton(buttonOK);

        buttonOK.addActionListener(e -> onOK());

        // call onCancel() when cross is clicked
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                onCancel();
            }
        });

        feedbackButton.addActionListener(e -> {
           var selectedRow = assignmentTable.getSelectedRow();
           if (selectedRow > -1) {
               var d = this.data.get(selectedRow);
               var message = JOptionPane.showInputDialog(this,"Enter your Feedback");
               if (message.isEmpty()) {
                   return;
               }
               StudentAssignmentSetFeedbackResponse response = Run.msg.send(new StudentAssignmentSetFeedbackRequest(d.getId(),message));
                if (response.isSuccess()) {
                    JOptionPane.showMessageDialog(this,"Feedback Saved");
                    getData(assignmentId);
                }
                else {
                    JOptionPane.showMessageDialog(this,response.getMessage());
                }
           }
        });
        // call onCancel() on ESCAPE
        contentPane.registerKeyboardAction(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                onCancel();
            }
        }, KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        getData(assignmentId);
    }

    private void getData(int assignmentId) {
       StudentAssignmentViewResponse response =  Run.msg.send(new StudentAssignmentViewRequest(assignmentId,0));
        if (response.isSuccess()) {
            data = response.getData();
            assignmentTable.setModel(new StudentAssignmentTableModel(data));
        }
        else {
            JOptionPane.showMessageDialog(this,response.getMessage());
        }
    }

    private void onOK() {
        // add your code here
        dispose();
    }

    private void onCancel() {
        // add your code here if necessary
        dispose();
    }

}
