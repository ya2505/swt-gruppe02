package gui.main.teacher;

import connection.*;
import general.AppSession;
import general.Run;
import models.StudentGroup;

import javax.swing.*;

public class TeacherGroupForm {
    public JPanel main;
    private JButton createButton;
    private JButton viewButton;
    private JButton deleteButton;
    private JList<StudentGroup> groupList;

    public TeacherGroupForm() {
        createButton.addActionListener(e -> {
            TeacherGroupCreateDialog dialog = new TeacherGroupCreateDialog();
            dialog.pack();
            dialog.setSize(300,300);
            dialog.setVisible(true);
            this.getData();
        });
        viewButton.addActionListener(e -> {
            var items = this.groupList.getSelectedValuesList();
            if (items.isEmpty()) {
                return;
            }
            TeacherGroupViewDialog dialog = new TeacherGroupViewDialog(items.get(0).getId());
            dialog.pack();
            dialog.setSize(300,300);
            dialog.setVisible(true);
        });
        deleteButton.addActionListener(e -> {
            var items = this.groupList.getSelectedValuesList();
            if (items.isEmpty()) {
                return;
            }
            var result = JOptionPane.showConfirmDialog(this.main,"Do you want to Delete?","Confirm",JOptionPane.OK_CANCEL_OPTION);
            if (result == 0){
                StudentGroupDeleteResponse res = Run.msg.send(new StudentGroupDeleteRequest(items.get(0).getId()));
                if (res.isSuccess()) {
                    getData();
                }
                else {
                    JOptionPane.showMessageDialog(this.main,res.getMessage());
                }
            }
        });
        getData();
    }
    private void getData() {
        StudentGroupGetResponse response = Run.msg.send(new StudentGroupGetRequest(AppSession.Person.getId(), 0));
        if (response.isSuccess()) {
            var data = response.getData();
            var model = new DefaultListModel<StudentGroup>();
            model.addAll(data);
            this.groupList.setModel(model);
        }
    }

}
