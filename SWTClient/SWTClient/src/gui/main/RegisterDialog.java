package gui.main;

import connection.RegisterRequest;
import connection.RegisterResponse;
import general.Run;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

public class RegisterDialog  extends SharedForm{
    private JTextField txtName;
    private JTextField txtEmail;
    private JPasswordField txtPassword;
    private JComboBox cbxRole;
    private JButton saveButton;
    public JPanel registerPanel;
    private JButton cancelButton;

    public RegisterDialog() {
        cancelButton.addActionListener(e -> {
            setJFrame(e);
            jFrame.setTitle("CaseMaster - Login");
            jFrame.setContentPane(new LoginForm().loginPanel);
            jFrame.pack();
            jFrame.setLocationRelativeTo(null);
            jFrame.setSize(400,300);
            jFrame.setVisible(true);
        });
        saveButton.addActionListener(e -> {
            setJFrame(e);
           RegisterResponse response = Run.msg.send(new RegisterRequest(txtName.getText(),txtEmail.getText(),cbxRole.getSelectedItem().toString(),String.valueOf(txtPassword.getPassword())));
           if (response.isSuccess()) {
               txtName.setText("");
               txtPassword.setText("");
               txtEmail.setText("");
               cbxRole.setSelectedIndex(0);
               JOptionPane.showMessageDialog(jFrame,"Register Success");
           }
           else {
               JOptionPane.showMessageDialog(jFrame,response.getMessage());
           }
        });
    }
}
