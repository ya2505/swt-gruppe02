package models;

import connection.Response;


public class StudentAssignment extends Response {
    private int id;
    private int assignmentId;
    private int studentId;
    private int groupId;
    private String text;
    private String name;
    private String feedback;
    private boolean isComplete;
    private String answer;


    public StudentAssignment(int id, int assignmentId, int studentId, int groupId, String text, String name, String feedback, boolean isComplete, String answer) {
        this.id = id;
        this.assignmentId = assignmentId;
        this.studentId = studentId;
        this.groupId = groupId;
        this.text = text;
        this.name = name;
        this.feedback = feedback;
        this.isComplete = isComplete;
        this.answer = answer;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(int assignmentId) {
        this.assignmentId = assignmentId;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isComplete() {
        return isComplete;
    }

    public void setComplete(boolean complete) {
        isComplete = complete;
    }
}

