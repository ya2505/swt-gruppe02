package main;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import connection.*;
import general.Pair;
import models.Assignment;
import models.Person;
import models.StudentAssignment;
import models.StudentGroup;

public class DB_Connection {
	
	private final String url = "jdbc:mysql://localhost:3306/casemaster";
	private final String user = "root";
	private final String password = "root";
	public StudentAssignmentSetFeedbackResponse studentAssignmentSetFeedback(StudentAssignmentSetFeedbackRequest request) {
		try {
			Connection con = DriverManager.getConnection(url, user, password);
			Statement stmt = con.createStatement();
			var result = stmt.execute("Update studentassignment Set Feedback='" + request.getFeedback() + "' Where Id=" + request.getAssignmentId());
			if (stmt.getUpdateCount() > 0) {
				return new StudentAssignmentSetFeedbackResponse();
			}
			return new StudentAssignmentSetFeedbackResponse("Update Count is 0");
		}
		catch (Exception e) {
			e.printStackTrace();
			return new StudentAssignmentSetFeedbackResponse(e.getMessage());
		}
	}
	public StudentAssignmentSetCompleteResponse studentAssignmentSetComplete(StudentAssignmentSetCompleteRequest request) {
		try {
			Connection con = DriverManager.getConnection(url, user, password);
			Statement stmt = con.createStatement();
			var result = stmt.execute("Update studentassignment Set isComplete=1, Answer='"  + request.getAnswer() +"' Where Id=" + request.getAssignmentId());
			if (stmt.getUpdateCount() > 0) {
				return new StudentAssignmentSetCompleteResponse();
			}
			return new StudentAssignmentSetCompleteResponse("Update Count is 0");
		}
		catch (Exception e) {
			e.printStackTrace();
			return new StudentAssignmentSetCompleteResponse(e.getMessage());
		}
	}
	public StudentAssignmentViewByGroupResponse studentAssignmentViewByGroup(StudentAssignmentViewByGroup request) {
		try {
			Connection con = DriverManager.getConnection(url, user, password);
			Statement stmt = con.createStatement();
			var result = stmt.executeQuery("Select assignment.Id,assignment.Text,`group`.Id as GroupId,`group`.Name From assignment " +
					"Inner join `group` on `group`.Id = assignment.GroupId " +
					"Where assignment.TeacherId = " + request.getTeacherId() + ";");
			var list = new ArrayList<Assignment>();
			while (result.next()) {
				var assignment = new Assignment(result.getInt("Id"),
						result.getString("Text"),
						result.getInt("GroupId"),
						result.getString("Name")
				);
				list.add(assignment);
			}
			return new StudentAssignmentViewByGroupResponse(list);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new StudentAssignmentViewByGroupResponse(e.getMessage());
		}
	}
	public StudentAssignmentViewResponse studentAssignmentView(StudentAssignmentViewRequest request) {
		try {
			Connection con = DriverManager.getConnection(url, user, password);
			Statement stmt = con.createStatement();
			ResultSet result;
			if (request.getStudentId() == 0) {
				result = stmt.executeQuery("Select studentassignment.*,assignment.Text, person.name,person.email From studentassignment " +
						"Inner join assignment on assignment.Id = studentassignment.AssignmentId " +
						"Inner join person on person.Id = studentassignment.StudentId " +
						"Where studentassignment.AssignmentId =" + request.getId()+ ";");

			}
			else {
				result = stmt.executeQuery("Select studentassignment.*,assignment.Text,person.name,person.email From studentassignment " +
						"Inner join assignment on assignment.Id = studentassignment.AssignmentId " +
						"Inner join person on person.Id = studentassignment.StudentId " +
						"Where studentassignment.StudentId =" + request.getStudentId()+ ";");
			}
			var list = new ArrayList<StudentAssignment>();
			while (result.next()) {
				var assignment = new StudentAssignment(result.getInt("Id"),
						result.getInt("AssignmentId"),
						result.getInt("StudentId"),
						0,
						result.getString("Text"),
						result.getString("Name") + " - " + result.getString("Email"),
						result.getString("Feedback"),
						result.getBoolean("IsComplete"),
						result.getString("Answer")
						);
				list.add(assignment);
			}
			return new StudentAssignmentViewResponse(list);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new StudentAssignmentViewResponse(e.getMessage());
		}
	}
	public StudentAssignmentDeleteResponse studentAssignmentDelete(StudentAssignmentDeleteRequest request) {
		try {
			Connection con = DriverManager.getConnection(url, user, password);
			Statement stmt = con.createStatement();
			stmt.execute("Delete From assignment Where Id = " + request.getId()  +";");
			stmt.execute("Delete From `studentassignment` Where AssignmentId = " + request.getId()  +";");
			return new StudentAssignmentDeleteResponse();
		}
		catch (Exception e) {
			e.printStackTrace();
			return new StudentAssignmentDeleteResponse(e.getMessage());
		}
	}
	public StudentAssignmentCreateResponse studentAssignmentCreate(StudentAssignmentCreateRequest request) {
		try {
			Connection con = DriverManager.getConnection(url, user, password);
			for (int i = 0; i < request.getGroupIds().size(); i++) {
				var groupId = request.getGroupIds().get(i);
				String query = String.format("INSERT INTO `casemaster`.`assignment`\n" +
								"(`TeacherId`,\n" +
								"`Text`,`GroupId`)\n" +
								"VALUES (%s, '%s','%s');",
						request.getTeacherId(),
						request.getText(),
						groupId
						);
				PreparedStatement stmt = con.prepareStatement(query,
						Statement.RETURN_GENERATED_KEYS);
				stmt.executeUpdate();
				var keys = stmt.getGeneratedKeys();
				var assignmentId = 0;
				if (keys.next()) {
					assignmentId = keys.getInt(1);
				}
				keys.close();
				stmt.close();
				var result = con.createStatement().executeQuery("Select * From studentgroup Where GroupId = " + groupId + ";");
				var studentIds = new ArrayList<Integer>();
				while (result.next()) {
					studentIds.add(result.getInt("StudentId"));
				}
				for (Integer studentId : studentIds) {
					query = String.format("INSERT INTO `casemaster`.`studentassignment`\n" +
									"(`AssignmentId`,\n" +
									"`StudentId`,`IsComplete`)\n" +
									"VALUES (%s,%s,%s);",
							assignmentId,
							studentId,
							0
					);
					stmt = con.prepareStatement(query,
							Statement.RETURN_GENERATED_KEYS);
					stmt.executeUpdate();
				}
			}
			return new StudentAssignmentCreateResponse();
		}
		catch (Exception e) {
			e.printStackTrace();
			return new StudentAssignmentCreateResponse(e.getMessage());
		}
	}
	public StudentGroupGetResponse studentGroupGet(StudentGroupGetRequest request) {
		try {
			Connection con = DriverManager.getConnection(url, user, password);
			Statement stmt = con.createStatement();
			ResultSet rs;
			if (request.getStudentId() > 0) {
				rs = stmt.executeQuery("Select `group`.Id as 'Id', `group`.Name as 'Name', person.Id as 'TeacherId', person.Name as 'TeacherName' From `group`\n" +
						"inner join person on `group`.TeacherId = person.Id\n" +
						"inner join studentgroup on studentgroup.GroupId = `group`.Id\n" +
						"Where studentgroup.StudentId = " + request.getStudentId() + ";");
			}
			else {
				rs = stmt.executeQuery("Select `group`.Id as 'Id', `group`.Name as 'Name', person.Id as 'TeacherId', person.Name as 'TeacherName' From `group`\n" +
						"inner join person on `group`.TeacherId = person.Id\n" +
						"Where `group`.TeacherId = " + request.getTeacherId() + ";");
			}
			var list = new ArrayList<StudentGroup>();
			while (rs.next()) {
				list.add(new StudentGroup(rs.getInt("Id"),rs.getString("Name"),rs.getInt("TeacherId"),
						rs.getString("TeacherName")));
			}
			return new StudentGroupGetResponse(list);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new StudentGroupGetResponse(e.getMessage());
		}
	}
	public StudentGroupDeleteResponse studentGroupDelete(StudentGroupDeleteRequest request){
		try {
			Connection con = DriverManager.getConnection(url, user, password);
			Statement stmt = con.createStatement();
			stmt.execute("Delete From studentgroup Where GroupId = " + request.getGroupId()  +";");
			stmt.execute("Delete From `group` Where Id = " + request.getGroupId()  +";");
			return new StudentGroupDeleteResponse();
		}
		catch (Exception e) {
			e.printStackTrace();
			return new StudentGroupDeleteResponse(e.getMessage());
		}
	}
	public StudentGroupCreateResponse studentGroupCreate(StudentGroupCreateRequest request) {
		try {
			Connection con = DriverManager.getConnection(url, user, password);
			String query = String.format("INSERT INTO `casemaster`.`group`\n" +
							"(`TeacherId`,\n" +
							"`Name`)\n" +
							"VALUES (%s, '%s');",
					request.getTeacherId(),
					request.getName()
			);
			PreparedStatement stmt = con.prepareStatement(query,
					Statement.RETURN_GENERATED_KEYS);
			stmt.executeUpdate();
			var keys = stmt.getGeneratedKeys();
			var groupId = 0;
			if (keys.next()) {
				groupId = keys.getInt(1);
			}
			keys.close();
			stmt.close();
			List<Integer> studentIds = request.getStudentIds();
			for (int i = 0; i < studentIds.size(); i++) {
				Integer studentId = studentIds.get(i);
				query = String.format("INSERT INTO `casemaster`.`studentgroup`\n" +
								"(`StudentId`,\n" +
								"`GroupId`)\n" +
								"VALUES (%s, %s);",
						studentId,
						groupId
				);
				stmt = con.prepareStatement(query);
				stmt.executeUpdate();
			}
			return new StudentGroupCreateResponse();
		}
		catch (Exception e) {
			e.printStackTrace();
			return new StudentGroupCreateResponse(e.getMessage());
		}
	}
	public GetStudentsByGroupIdResponse getStudentsByGroupId(GetStudentsByGroupIdRequest request) {
		try {
			Connection con = DriverManager.getConnection(url, user, password);
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("Select Id,Name,Email,Role From studentgroup\n" +
					"inner join person on studentgroup.StudentId = person.Id\n" +
					"Where studentgroup.GroupId = " + request.getGroupId() + ";");
			var list = new ArrayList<Person>();
			while (rs.next()) {
				list.add(new Person(rs.getInt("Id"),rs.getString("Name"),rs.getString("Email"),rs.getString("Role")));
			}
			return new GetStudentsByGroupIdResponse(list);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new GetStudentsByGroupIdResponse(e.getMessage());
		}
	}
	public GetAllStudentsResponse getAllStudents(GetAllStudentsRequest request) {
		try {
			Connection con = DriverManager.getConnection(url, user, password);
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from Person Where Role = 'Student'");
			var list = new ArrayList<Person>();
			while (rs.next()) {
				list.add(new Person(rs.getInt("Id"),rs.getString("Name"),rs.getString("Email"),rs.getString("Role")));
			}
			return new GetAllStudentsResponse(list);
		}
		catch (Exception e) {
			e.printStackTrace();
			return new GetAllStudentsResponse(e.getMessage());
		}
	}
	public RegisterResponse register(RegisterRequest request) {
		try {
			Connection con = DriverManager.getConnection(url, user, password);
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from Person Where Email = '" + request.getEmail() +"'");
			if(rs.next()) {
				return new RegisterResponse("Email already exists");
			}
			 String query = String.format("INSERT INTO `casemaster`.`person` (`Name`, `Email`, `Password`, `Role`) " +
							 "VALUES ('%s', '%s', '%s', '%s');",
					 request.getName(),
					 request.getEmail(),
					 request.getPassword(),
					 request.getRole()
			 );
			stmt.execute(query);
			return new RegisterResponse();
		}
		catch (Exception e) {
			e.printStackTrace();
			return new RegisterResponse(e.getMessage());
		}
	}
	public LoginResponse login(LoginRequest request) {
		try {
			Connection con = DriverManager.getConnection(url, user, password);
			Statement stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery("select * from Person");
			
			while (rs.next()) {
				if (request.getUserName().equals(rs.getString("Email")) && request.getPassword().equals(rs.getString("Password")))
						return new LoginResponse(new Person(
								rs.getInt("Id"),
								rs.getString("Name"),
								rs.getString("Email"),
								rs.getString("Role")
								));
			}
			
			con.close();
			return new LoginResponse("Invalid Username/Pass");
			
		} catch(Exception e) { 
			e.printStackTrace();
			return new LoginResponse(e.getMessage());
		}
	}

	public ForgetPasswordResponse forgetPassword(ForgetPasswordRequest req) {
		try {
			Connection con = DriverManager.getConnection(url, user, password);
			Statement stmt = con.createStatement();
			String query = String.format("UPDATE `casemaster`.`person` SET Password= '%s' Where Email = '%s'",req.getUserName(),req.getPassword());
			stmt.execute(query);
			int numUpdates = stmt.getUpdateCount();
			if (numUpdates == 1) {
				return new ForgetPasswordResponse();
			}
			return new ForgetPasswordResponse("Invalid Email");
		}
		catch (Exception e) {
			e.printStackTrace();
			return new ForgetPasswordResponse(e.getMessage());
		}
	}
}