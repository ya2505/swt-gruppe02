package main;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

import connection.*;
import general.*;

public class ServerMessager implements Runnable {
	private ServerSocket serverSocket;
	
	public ServerMessager(int port) {
		try {
			serverSocket = new ServerSocket(port);
		} catch (Exception e) { e.printStackTrace(); }
	}
	
	// Hauptloop, wartet auf Verbindung von Clients und erzeugt ClientHandlers f�r diese
	public void run() {
		while (true) {
			try {
				ClientHandler handler = new ClientHandler(serverSocket.accept());
				new Thread(handler).start();
			} catch (Exception e) { e.printStackTrace(); }
		}
	}

	// Verwaltet die Verbindung zu einem Client (mehrere pro Server)
	private class ClientHandler implements Runnable {
		private Socket client;
		private ObjectInputStream in;
		private ObjectOutputStream out;

		public ClientHandler(Socket socket) {
			client = socket;
			
			try {
				in = new ObjectInputStream(client.getInputStream());
				out = new ObjectOutputStream(client.getOutputStream());
			} catch (Exception e) { e.printStackTrace(); }
		}

		@Override
		public void run() {
			Request request = null;
			
			try {
				while ((request = (Request) in.readObject()) != null) {
					if (request instanceof LoginRequest req) {
						var res = Run.db.login(req);
						out.writeObject(res);
					}
					else if (request instanceof RegisterRequest req) {
						var res = Run.db.register(req);
						out.writeObject(res);
					}
					else if (request instanceof ForgetPasswordRequest req) {
						ForgetPasswordResponse res = Run.db.forgetPassword(req);
						out.writeObject(res);
					}
					else if (request instanceof GetAllStudentsRequest req) {
						var res = Run.db.getAllStudents(req);
						out.writeObject(res);
					}
					else if (request instanceof GetStudentsByGroupIdRequest req) {
						var res = Run.db.getStudentsByGroupId(req);
						out.writeObject(res);
					}
					else if (request instanceof StudentGroupCreateRequest req) {
						var res = Run.db.studentGroupCreate(req);
						out.writeObject(res);
					}
					else if (request instanceof StudentGroupDeleteRequest req) {
						var res = Run.db.studentGroupDelete(req);
						out.writeObject(res);
					}
					else if (request instanceof StudentGroupGetRequest req) {
						var res = Run.db.studentGroupGet(req);
						out.writeObject(res);
					}
					else if (request instanceof StudentAssignmentCreateRequest req) {
						var res = Run.db.studentAssignmentCreate(req);
						out.writeObject(res);
					}
					else if (request instanceof StudentAssignmentDeleteRequest req) {
						var res = Run.db.studentAssignmentDelete(req);
						out.writeObject(res);
					}
					else if (request instanceof StudentAssignmentViewRequest req) {
						var res = Run.db.studentAssignmentView(req);
						out.writeObject(res);
					}
					else if (request instanceof StudentAssignmentViewByGroup req) {
						var res = Run.db.studentAssignmentViewByGroup(req);
						out.writeObject(res);
					}
					else if (request instanceof StudentAssignmentSetCompleteRequest req) {
						var res = Run.db.studentAssignmentSetComplete(req);
						out.writeObject(res);
					}
					else if (request instanceof StudentAssignmentSetFeedbackRequest req) {
						var res = Run.db.studentAssignmentSetFeedback(req);
						out.writeObject(res);
					}
				}
			} catch (Exception e) { e.printStackTrace(); }
		}

	}
}
