package connection;

public class ForgetPasswordResponse extends BaseResponse<String> {
    public ForgetPasswordResponse() {
        super();
    }

    public ForgetPasswordResponse(String message) {
        super(null, message,false);
    }

    public ForgetPasswordResponse(String data, String message, boolean isSuccess) {
        super(data, message, isSuccess);
    }
}
