package connection;

public class StudentGroupDeleteResponse extends BaseResponse<String> {

    public StudentGroupDeleteResponse() {
    }

    public StudentGroupDeleteResponse(String message) {
        super(null, message, false);
    }

    public StudentGroupDeleteResponse(String data, String message, boolean isSuccess) {
        super(data, message, isSuccess);
    }
}
