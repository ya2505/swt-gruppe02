package connection;


public class StudentGroupGetRequest extends BaseRequest {
    private int teacherId;
    private int studentId;

    public StudentGroupGetRequest(int teacherId, int studentId) {
        this.teacherId = teacherId;
        this.studentId = studentId;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }
}
