package connection;

public class StudentAssignmentSetCompleteResponse extends BaseResponse<Void> {
    public StudentAssignmentSetCompleteResponse() {
    }

    public StudentAssignmentSetCompleteResponse(Void data) {
        super(data);
    }

    public StudentAssignmentSetCompleteResponse(String message) {
        super(message);
    }

    public StudentAssignmentSetCompleteResponse(Void data, String message, boolean isSuccess) {
        super(data, message, isSuccess);
    }
}
