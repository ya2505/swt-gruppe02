package connection;

import models.Person;

import java.util.List;

public class GetStudentsByGroupIdResponse extends BaseResponse<List<Person>> {
    public GetStudentsByGroupIdResponse() {
    }

    public GetStudentsByGroupIdResponse(List<Person> data) {
        super(data);
    }

    public GetStudentsByGroupIdResponse(String message) {
        super(message);
    }

    public GetStudentsByGroupIdResponse(List<Person> data, String message, boolean isSuccess) {
        super(data, message, isSuccess);
    }
}
