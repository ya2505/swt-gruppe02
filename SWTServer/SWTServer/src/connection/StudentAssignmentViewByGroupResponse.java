package connection;

import models.Assignment;

import java.util.List;

public class StudentAssignmentViewByGroupResponse extends BaseResponse<List<Assignment>> {

    public StudentAssignmentViewByGroupResponse() {
    }

    public StudentAssignmentViewByGroupResponse(List<Assignment> data) {
        super(data);
    }

    public StudentAssignmentViewByGroupResponse(String message) {
        super(message);
    }

    public StudentAssignmentViewByGroupResponse(List<Assignment> data, String message, boolean isSuccess) {
        super(data, message, isSuccess);
    }
}
