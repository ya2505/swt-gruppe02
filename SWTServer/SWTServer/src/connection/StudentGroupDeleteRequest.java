package connection;

public class StudentGroupDeleteRequest extends BaseRequest {
    private int groupId;

    public StudentGroupDeleteRequest(int groupId) {
        this.groupId = groupId;
    }

    public int getGroupId() {
        return groupId;
    }

    public void setGroupId(int groupId) {
        this.groupId = groupId;
    }
}
