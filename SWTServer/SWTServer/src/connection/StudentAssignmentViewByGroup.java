package connection;

public class StudentAssignmentViewByGroup extends BaseRequest {
    private int teacherId;

    public StudentAssignmentViewByGroup(int teacherId) {
        this.teacherId = teacherId;
    }

    public int getTeacherId() {
        return teacherId;
    }

    public void setTeacherId(int teacherId) {
        this.teacherId = teacherId;
    }
}
