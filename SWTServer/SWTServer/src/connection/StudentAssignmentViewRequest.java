package connection;

public class StudentAssignmentViewRequest extends BaseRequest {
    private int id;
    private int studentId;


    public StudentAssignmentViewRequest(int id, int studentId) {
        this.id = id;
        this.studentId = studentId;
    }

    public int getStudentId() {
        return studentId;
    }

    public void setStudentId(int studentId) {
        this.studentId = studentId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
