package connection;

public class StudentAssignmentSetFeedbackRequest extends BaseRequest {
    private int assignmentId;
    private String feedback;

    public StudentAssignmentSetFeedbackRequest(int assignmentId, String feedback) {
        this.assignmentId = assignmentId;
        this.feedback = feedback;
    }

    public int getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(int assignmentId) {
        this.assignmentId = assignmentId;
    }

    public String getFeedback() {
        return feedback;
    }

    public void setFeedback(String feedback) {
        this.feedback = feedback;
    }
}
