package connection;

public class StudentAssignmentSetCompleteRequest extends BaseRequest {
    private int assignmentId;
    private String answer;

    public StudentAssignmentSetCompleteRequest(int assignmentId, String answer) {
        this.assignmentId = assignmentId;
        this.answer = answer;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getAssignmentId() {
        return assignmentId;
    }

    public void setAssignmentId(int assignmentId) {
        this.assignmentId = assignmentId;
    }
}
