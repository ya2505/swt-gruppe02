package connection;

public class StudentAssignmentCreateResponse extends BaseResponse<Void> {

    public StudentAssignmentCreateResponse() {
    }

    public StudentAssignmentCreateResponse(Void data) {
        super(data);
    }

    public StudentAssignmentCreateResponse(String message) {
        super(message);
    }

    public StudentAssignmentCreateResponse(Void data, String message, boolean isSuccess) {
        super(data, message, isSuccess);
    }
}
