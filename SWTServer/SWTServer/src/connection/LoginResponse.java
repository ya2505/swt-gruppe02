package connection;

import models.Person;

public class LoginResponse extends BaseResponse<Person> {

    public LoginResponse() {
        super();
    }

    public LoginResponse(Person data) {
        super(data);
    }

    public LoginResponse(String message) {
        super(message);
    }

    public LoginResponse(Person data, String message, boolean isSuccess) {
        super(data, message, isSuccess);
    }
}
